const Show = require('./users/show.js')
const Create = require('./users/create.js')

module.exports = {
  users: {
    Show
  },
  user: {
    Create
  }
}
